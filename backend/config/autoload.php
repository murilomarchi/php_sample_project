<?php

spl_autoload_register(function($class_name) {
    $directories = array();
    $directories[] = dirname(__FILE__) . '/../controllers/' . $class_name . '.php';
    $directories[] = dirname(__FILE__) . '/../models/' . $class_name . '.php';
    
    foreach($directories as $directory){
        if (file_exists($directory)){
            include_once $directory;
            break;
        }
    }
});        
