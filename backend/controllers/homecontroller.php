<?php
class homecontroller{

    function __construct($params){        
        switch ($params['operation']){
            case 'login':
                if(!isset($params['username'])){
                    echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
                    exit;
                }
                
                $user = new User($params['username']);
                echo $user->response;
            break;
// ----------------------------------------------------------------------------------------------------
            case 'create':
                $user = new User();
                $user->username = $params['username'];
                $user->saveUser();
                echo $user->response;

            break;
// ----------------------------------------------------------------------------------------------------
            case 'delete':
                $user = new User();
                $user->userid = $params['userid'];
                $user->deleteUser();
                echo $user->response;
            break;
        }
    }
}
