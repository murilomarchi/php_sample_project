<?php
try{
    include_once dirname(__FILE__) . "/config/autoload.php";

    if(!isset($_POST['operation'])){
        echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
        exit;
    }

    new homecontroller($_POST);
    
}catch (Exception $e){
    var_dump($e);
}