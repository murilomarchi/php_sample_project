<?php
class User{
    public  $userid         = null;
    public  $username       = null;
    public  $useroptions    = null;
    private $connection     = null;
    public  $response       = null;

    function __construct($username = null){   
        //connect to local Database
        $this->connection = new DBConnect();
 
        if(!is_null($username)){
            $this->checkUser($username);        
        }           
    }

    public function saveUser(){
        $sql = 'INSERT INTO users (username, options) VALUES (:username, :options) '.
                ' ON DUPLICATE KEY UPDATE '. 
                ' options=:options';
        $stmt = $this->connection->db->prepare($sql);
        $stmt->bindValue(':username', $this->username);
        $stmt->bindValue(':options' , $this->useroptions);
        $result = $stmt->execute();
        if(!$result){
            $this->response = json_encode(array('status'=> 'ERROR', 'reason' => 'SQL_ERROR'));
        }else{            
            $user = array('userid' => $stmt->insert_id, 'username' => $this->username, 'options' => $this->options);
            $this->response = json_encode(array('status'=> 'OK', 'reason' => 'USER_CREATED', 'user'=> $user));         
        }
    }

    public function deleteUser(){
        $sql = 'DELETE FROM users WHERE userid = :userid';
        $stmt = $this->connection->db->prepare($sql);
        $stmt->bindValue(':userid', $this->userid);
        $result = $stmt->execute();
        if(!$result){
            $this->response = json_encode(array('status'=> 'ERROR', 'reason' => 'SQL_ERROR'));
        }else{            
            $user = array('userid' => $stmt->insert_id, 'username' => $this->username, 'options' => $this->options);
            $this->response = json_encode(array('status'=> 'OK', 'reason' => 'USER_DELETED'));         
        }
    }


    public function checkUser($username){
        $sql = 'SELECT * FROM users u WHERE u.username = :username';
        $stmt = $this->connection->db->prepare($sql);
        $stmt->bindValue(':username', $username);
        $result = $stmt->execute();
        if(!$result){
            $this->response = json_encode(array('status'=> 'ERROR', 'reason' => 'SQL_ERROR'));
        }else{            
            if($stmt->rowCount() == 0){
                $this->response = json_encode(array('status'=> 'OK', 'reason' => 'USER_NOT_FOUND', 'username' => $username));
            }else{
                $row = $stmt->fetch();
                $user = array('userid' => $row['userid'], 'username' => $row['username'], 'options' => $row['options']);
                $this->response = json_encode(array('status'=> 'OK', 'reason' => 'USER_FOUND', 'user'=> $user));
            }            
        }
    }
}
