<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="./src/assets/main.js"></script>
    <link rel="stylesheet" type="text/css" href="./src/css/main.css">

    <!-- Modal Code -->
    <div id="modal" class="modal">
        <div class="modal-content">
            <span class="close" onclick="mainjs.closeModal()">&times;</span>
            <p id="modal_content"></p>
            <button onclick="location.href='./';">OK</button>
        </div>
    </div>    
    <!-- End Modal Code -->

    <title>Murilo Login</title>
  </head>
  <body>
    <?php require_once dirname(__FILE__) . '/src/assets/auth.php';?>
  </body>
</html>