<?php
session_start();

if(isset($_SESSION['user'])){
    //logged user
    require_once dirname(__FILE__) . '/../../screens/logged_user.php';
}elseif(isset($_SESSION['new_user'])){
    //create new user
    require_once dirname(__FILE__) . '/../../screens/new_user.php';
}else{
    //login window
    require_once dirname(__FILE__) . '/../../screens/login.php';
}
