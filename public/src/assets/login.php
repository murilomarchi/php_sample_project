<?php
try{
    //sendResponse(200, json_encode(array('status' => 'OK')), 'application/json');
    if(!isset($_POST['operation'])){
        echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
        exit;
    }

    switch ($_POST['operation']){
        case 'login':
            if(!isset($_POST['username']) || strlen($_POST['username']) == 0){
                echo json_encode(array('status'=>'ERROR', 'reason'=>'INVALID_USERNAME'));
                exit;
            }
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,'http://localhost/php/backend/');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('username'=>$_POST['username'], 'operation' => $_POST['operation'])));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec($ch);
            
            curl_close($ch);

            $server_output = json_decode($server_output);

            if($server_output->status == 'OK'){
                if($server_output->reason == 'USER_FOUND'){
                    session_start();
                    $_SESSION['user'] = $server_output->user;     
                    
                    //redirect user to logged user area
                    echo json_encode(array('status'=>'OK', 'reason'=>'login'));
                }elseif($server_output->reason == 'USER_NOT_FOUND'){
                    session_start();
                    $_SESSION['new_user'] = $server_output->username;  

                    //redirect user to register area
                    echo json_encode(array('status'=>'OK', 'reason'=>'new_user'));

                }
            }else{
                echo json_encode(array('status'=>'ERROR', 'reason'=> 'LOGIN_ERROR'));
            }
        break;
// ----------------------------------------------------------------------------------------------------
    case 'logout':
        session_start();
        session_destroy();
        echo json_encode(array('status'=>'OK'));
    break;


// ----------------------------------------------------------------------------------------------------
        default:
            echo json_encode(array('status'=>'ERROR', 'reason'=>'UNKNOWN_OPERATION'));
            exit;
        break;
    }        
    exit;
}catch (Exception $e){
    var_dump($e);
}
    