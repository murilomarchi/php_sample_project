var mainjs = {
    closeModal: function() {
        $('#modal').css('display', 'none');
    },
    openModal: function(code){
        var text = code;
        if(code == 'MISSING_PARAMS'){
            text = 'Missing Required Parameters';
        }else if(code == 'INVALID_USERNAME'){
            text = 'Please insert a valid Username';
        }else if(code == 'new_user'){
            text = 'User Not found, Redirecting to Sing Up area ';
        }else if(code == 'LOGIN_ERROR'){
            text = 'Internal Error';
        }else if(code == 'UNABLE_TO_CREATE_USER'){
            text = 'Unable to create a new user';
        }else if(code == 'CREATE_ERROR'){
            text = 'Error while creating a new user';
        }else if(code == 'USER_CREATED'){
            text = 'User Created, logging in ...'
        }else if(code == 'USER_DELETED'){
            text = 'User Deleted'
        }

        $('#modal_content').html(text);
        $('#modal').css('display', 'block');
    },
    loginSubmit: function(e) {
        e.preventDefault();
        var parameters = $('#login').serializeArray();
        var username = parameters[0].value;
        $.ajax({
            type: "POST",
            url: "./src/assets/login.php",
            data: {operation: 'login', username: username},
            dataType:'JSON', 
            success: function(response){
                if(response.status != 'OK'){
                    mainjs.openModal(response.reason);
                }else{
                    if(response.reason == 'login'){
                        location.href = './';
                    }else{
                        mainjs.openModal(response.reason);
                    }
                    
                    //console.log(response.result);
                }
                // put on console what server sent back...
            },
        });
    },
    logoutSubmit: function(e){
        $.ajax({
            type: "POST",
            url: "./src/assets/login.php",
            data: {operation: 'logout'},
            dataType:'JSON', 
            success: function(response){
                location.href = './';
            }
        }); 
    },
    createSubmit: function(e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "./src/assets/create.php",
            data: {operation: 'create', username: $('#username').val()},
            dataType:'JSON', 
            success: function(response){
                if(response.status != 'OK'){
                    mainjs.openModal(response.reason);
                }else{
                    mainjs.openModal(response.reason);
                    
                    //console.log(response.result);
                }
                // put on console what server sent back...
            },
        });
    },
    deleteSubmit: function(e){
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "./src/assets/create.php",
            data: {operation: 'delete', userid: $('#userid').val()},
            dataType:'JSON', 
            success: function(response){
                mainjs.openModal(response.reason);
            }
        }); 
    }
}